### skadnetworkitems

Tiny tool to merge plist files provided by adnetworks.

Remove duplicated ids.

##### Prerequisites:
- have go 1.14 or more recent installed.
- should work on any OS.

##### Install
`go install bitbucket.org/iscool_admin/skadnetworkitems@latest`

##### Usage:
`skadnetworkitems ./enabled/*.plist > networks.csv`
