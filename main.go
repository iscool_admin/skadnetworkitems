package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime/debug"
	"sort"
	"strings"

	"howett.net/plist"

	"github.com/alecthomas/kong"
)

var (
	cli struct {
		Files   []string `arg type:"existingfile"`
		Version kong.VersionFlag
	}

	header = []byte("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n<plist version=\"1.0\">\n\t<dict>")
	footer = []byte("</dict>\n</plist>")
)

type pListFile struct {
	SKAdNetworkItems []struct {
		SKAdNetworkIdentifier string
	}
}

func main() {
	infos, _ := debug.ReadBuildInfo()
	kong.Parse(&cli,
		kong.Description("processes ad providers plist files to produce a sorted list of unique skadnetworkitems"),
		kong.Vars{
			"version": infos.Main.Version,
		})
	ids := make(map[string]struct{}, 200)
	for _, f := range cli.Files {
		for _, i := range parseFile(f) {
			ids[strings.ToLower(i)] = struct{}{}
		}
	}
	slice := make([]string, 0, len(ids))
	for id := range ids {
		slice = append(slice, id)
	}
	sort.Strings(slice)
	for _, id := range slice {
		fmt.Fprintln(os.Stdout, id)
	}
}

func parseFile(filename string) []string {
	f, err := ioutil.ReadFile(filename)
	if err != nil {
		fatalError("error: reading %s: %s", filename, err)
	}
	buf := bytes.NewBuffer(header)
	buf.Write(f)
	buf.Write(footer)
	var data pListFile
	_, err = plist.Unmarshal(buf.Bytes(), &data)
	if err != nil {
		fatalError("error: parsing %s: %s", filename, err)
	}
	ids := make([]string, 0, len(data.SKAdNetworkItems))
	for _, i := range data.SKAdNetworkItems {
		if !strings.HasSuffix(i.SKAdNetworkIdentifier, ".skadnetwork") {
			log.Printf("error: invalid SKAdNetworkIdentifier: %s: %s", filename, i.SKAdNetworkIdentifier)
			continue
		}
		if len(i.SKAdNetworkIdentifier) < 21 {
			log.Printf("warning: suspicious SKAdNetworkIdentifier length: %s: %s", filename, i.SKAdNetworkIdentifier)
		}
		ids = append(ids, i.SKAdNetworkIdentifier)
	}
	return ids
}

func fatalError(format string, args ...interface{}) {
	_, _ = fmt.Fprintf(os.Stderr, format, args...)
	os.Exit(1)
}
