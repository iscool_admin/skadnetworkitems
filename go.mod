module bitbucket.org/iscool_admin/skadnetworkitems

go 1.14

require (
	github.com/alecthomas/kong v0.2.11
	github.com/kr/pretty v0.1.0 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.1 // indirect
	howett.net/plist v1.0.0
)
